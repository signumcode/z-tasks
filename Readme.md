# Showcase of Postgresql + Graphql + typescript + React tech stack

![Screenshot 1](screenshots/1.PNG?raw=true )


### Run all together with docker

#### In dev mode
To start
```
 cd ./infrastructure
 docker-compose -f docker-compose.dev.yml up  --build 
```
To run only selected service
```
  cd ./infrastructure
  docker-compose -f docker-compose.dev.yml  up db_postgres_dev
```
To stop
```
 cd ./infrastructure
 docker-compose -f docker-compose.dev.yml down
```

#### In production mode
To start
```
 cd ./infrastructure
 docker-compose -f docker-compose.prod.yml  up --build
```
To stop
```
 cd ./infrastructure
 docker-compose -f docker-compose.prod.yml down
```