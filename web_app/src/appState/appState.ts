import { makeVar } from '@apollo/client'
import { AccountMeQuery, GetBoardsQuery } from 'queries/types'


export const gSelectedBoard = makeVar<{
  id: string
  title: string
} | null>(null)

export const gUserMe = makeVar<AccountMeQuery['accountMe'] | null>(null)


export const gBoards = makeVar<GetBoardsQuery['boards'] | null>(null)
