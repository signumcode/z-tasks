import {
  ApolloClient,
  InMemoryCache,
  DefaultOptions,
  NormalizedCacheObject,
} from '@apollo/client'
import link from './link'

const defaultOptions: DefaultOptions = {
  watchQuery: {
    fetchPolicy: 'cache-and-network',
    errorPolicy: 'all',
  },
  query: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'all',
  }
}

const cache = new InMemoryCache()
export default new ApolloClient<NormalizedCacheObject>({
  uri: process.env.REACT_APP_GRAPHQL_URI,
  link,
  defaultOptions,
  cache,
  name: 'ZTasks',
  version: '0.1'
  // typeDefs
})
