const { getThemeVariables } = require('antd/dist/theme')
const { override, fixBabelImports, addLessLoader } = require('customize-cra')

module.exports = override()
