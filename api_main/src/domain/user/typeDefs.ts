import { gql } from 'apollo-server'

export default gql`
  type User {
    id: ID!
    fullName: String!
    email: EmailAddress!
    avatar: FileAttachment
  }


  extend type Query {
    userGetList: [User]!  @auth(requires: USER)
    userGetById(id: ID): User @auth(requires: USER)
  }

`
